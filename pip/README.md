Role pip
========

Installation de pip sur la machine distante pour l'installation des librairies Dockers.

Requirements
------------

utilisation des modules apt. Ce rôle n'est compatible qu'avec les Debian et dérivés.

Role Variables
--------------

aucunes variables pour ce rôle

tasks
-----

- Installation de pip par apt (nécésaire pour l'utilisation du module `pip`)
- installation des modules Python `docker` et `docker-compose` par pip

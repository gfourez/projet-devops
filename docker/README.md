Role docker
===========

Installation de Docker sur la machine cible

Requirements
------------

Utilisation des modules apt. Ce rôle n'est compatible qu'avec les Debian et dérivés.

Role Variables
--------------

- `debian_version`: `buster` par défaut

tasks
-----

Suivi des instructions d'installation depis le site [Docker](https://docs.docker.com/engine/install/debian/)

- Désinstallation des anciennes versions par `apt`
- Création du repo docker
  * Installation des prérequis par `apt`
  * Récupération de la clef GPG et ajout par `apt_key`
  * Ajout du repo par `apt_repository`
- installation de docker par `apt`

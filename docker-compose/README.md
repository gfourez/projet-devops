Role Name
=========

Installation de Docker-Compose

Requirements
------------

Pas de prérequis pour ce rôle

Role Variables
--------------

- `docker_compose_version`: `1.25.5` par défaut. La version de docker-compose à installer

tasks
-----

Suivi des instructions d'installation depis le site [Docker](https://docs.docker.com/compose/install/)

- Désinstallation d'une éventuelle ancienne versions par `file`. Cette étape évite une erreur lorsque ce fichier existe déjà
- téléchargement de l'executable docker-compose par `uri` dans `/usr/local/bin/docker-compose`. Ajout des droits d'execution
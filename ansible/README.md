Role ansible
===========

Installation de ansible pour Debian

Requirements
------------

Utilisation des modules `apt`. Ce rôle n'est compatible qu'avec les Debian et dérivés.

Role Variables
--------------



tasks
-----

Suivi des instructions d'installation pour Debian depuis le site [ansible](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html#installing-ansible-on-debian)

- ajout du dépot dans `/etc/apt/sources.list` avec `lineinfile`
- ajout de la clef avec `apt-key`
- installation d'ansible avec `apt`
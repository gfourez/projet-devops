# Projet Devops

Installation de Gitlab et Jenkins sur une instance EC2 t2.medium par Ansible

## Choix de l'instance :

**t2.medium** guidé par les prérequis Gitlab et Jenkins.
- 4 Go de Ram pour Gitlab (https://docs.gitlab.com/ee/install/requirements.html#memory)
- 256 Mo pour jenkins

Le choix de la distribution a été porté sur Debian 10

## inventaire

Le groupe `ec2` a été créé pour pouvoir lui porter des variables :
- `ansible_ssh_private_key_file` : répertoire pour la clef privée de l'instance EC2. Elle a été placé à la racine du projet
- `ansible_python_interpreter` : on force l'utilisation de Python 3 sur la machine distante.

## Définition des Rôles :

### customize

Détails du rôle [ici](./customize/README.md)

### pip

Details du rôle [ici](./pip/README.md)

### Docker

Détails du rôle [ici](./docker/README.md)


### Docker-compose

details du rôle [ici](./docker-compose/README.md)

### ansible

details du rôle [ici](./ansible/README.md)

### Tools

Création du template docker-compose pour l'execution de Gitlab et Jenkins

Détails du rôle [ici](./tools/README.md)

## Auteurs

- Grégory Fourez
- Mahmoud Touati
- Michel Poualeu
- Sékou Sylla

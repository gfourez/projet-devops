Role customize
==============

Installation des outils shell : zsh, outils : htop, tmux, vim(+pathogène).

Requirements
------------

utilisation des modules apt. Ce rôle n'est compatible qu'avec les Debian et dérivés.

Role Variables
--------------

aucunes variables pour ce rôle

tasks
-----

- Installation de zsh et git (prérequis pour oh-my-zsh)
- Vérification de l'existance de zsh par la présence du fichier `~/.zshrc`
- Vérification de l'existance de oh-my-zsh par la présence du fichier `~/.oh-my-zsh`
- Clone du projet `oh-my-zsh` **si** oh-my-zsh n'existe pas déjà
- Récupération du template `.zshrc` **si** ce fichier n'existe pas déjà
- installation de tmux par apt
- installation de vim par apt
- installation de htop par apt
